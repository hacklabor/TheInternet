﻿Rote LED blinkt mit 0,8 s EIN + 0,8s aus im Batteriebetreib.
Sobald geladen wird (Laderegler Eingang = 5V --> PIN1 am ESP 2,5V HIGH) 
ändert sich der LED Interval auf 1,6s EIN und 1,6s AUS als Ladekontrolle ob
"TheInternet" auch richtig auf dem WIRELESS Lader aufliegt um den internen LIPO
3,7V zu laden.

Programiersprache LUA