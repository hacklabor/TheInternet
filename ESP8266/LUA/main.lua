-- Pin definition 
local pin = 2            --  LED Ausgang
local duration = 500    -- 0.5 second TIMER Interval
local last_state = 0
local cycles_std = 1
local cycles_chg = 10
local cycles = cycles_std
local led = true
local debug = false -- HIER EINSCHALTEN!

function set_led(status)
    gpio.write(pin, (status and gpio.HIGH or gpio.LOW))
end

function status_string(foo)
    -- 0 = entladen, 1 = laden
    return(foo==1 and "laden" or "ENTLADEN")
end

function hal_debug(...)
    if debug then 
        print(...)
    end
end



-- Initialising pin
gpio.mode(pin, gpio.OUTPUT)
set_led(false)
gpio.mode(1, gpio.INPUT) --PIN1 5V an Laderegler 

-- Create an interval
tmr.alarm(0, duration, 1, function ()
    current_state = gpio.read(1)
 
    if (current_state == last_state) then
        action = cycles > 1 and "konstant" or "blinking"
        hal_debug("Status bleibt bei " .. status_string(current_state) .. " " .. action)
    else
        cycles = cycles_chg
        led = (current_state==1)
        hal_debug("statuswechsel zu " .. status_string(current_state) .. " | cycles: ", cycles)
    end
    last_state = current_state

    cycles = cycles - 1
    if (cycles == 0) then
        led = not led
        cycles = cycles_std
    end

    set_led(led)
end)
